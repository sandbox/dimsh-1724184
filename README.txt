====================
JS/CSS Insert Module
====================
Allows administrators to insert custom CSS or Javascript into the page output
based on configurable rules, it also gives the option to override theme
stylesheets and javascript. It's useful for adding simple CSS tweaks without
modifying a site's theme every time, it helps themers and site builders to
quickly apply their CSS/JS additions using the admin interface especially when
displaying themeing the output of Views or a Panel page.

WARNING: This module will allow users with proper permissions to insert 
arbitrary CSS or Javascript code into the page which is considered dangerous 
from security point of view and may lead to web attacks if the individual who
is given the permissions is not trusted, the use of this module is considered
useful only in development stage, all the modifications this module allows
to the site interface should be placed in a seperated module or in the theme
on production websites.

The CSS files and JS files are added using Drupal's standard drupal_add_css()
and drupal_add_js() functions respectively, so CSS/JS aggregation is handled as
usual.

The module provides an option for rules to be loaded as *Theme* rules, a theme's
CSS/JS file are loaded after all modules CSS/JS files, further more, when a
*theme* rule defined by this module is loaded into the page, it will be loaded
*AFTER* the current theme's CSS/JS files, allowing the module admin to override
theme stylesheets and javascript.

This module is definitely not a replacement for full-fledged theming, but it
provides site administrators with a quick and easy way of tweaking things
without diving into full-fledged theme hacking.

Features:
=========
- Works well with CSS/JS aggregators, like Advanced CSS/JS Aggregation (advagg).
- Has a quick edit feature for changing the CSS/JS code definition using
  semi-Ajax interface (may add full-Ajax in the future), this feature works with
  block integration.
- Support for weighted rules, admin can change the weight of rules to control
  their load order.
- Admin can disable certain rules without deleting or commenting their code out.
- Rules may be loaded conditionally by pages, so for certain paths or path
  wildcards a rule may be set to 'active' or 'inactive' (similar to core block
  module's visibility).
- The CSS/JS code of a rule may optionally be parsed by PHP parser, and the
  output of the code evaluation will be the actual CSS or JS code to load into
  the page.
- For Javascript rules, supports choosing the scope ('header' or 'footer') and
  supports 'inline' Javascript.
- Any CSS/JS rule can be choosen not to be 'preprocessed' by Drupal, skipping
  the aggrigation (if enabled) for it.
- Can override theme's Stylesheets and Javascripts.


Installation:
=============
Enable the module at 'admin/build/modules', then go to
'admin/settings/jcss-insert' to list or add CSS/JS rules, a sample CSS rule is
placed by default which styles the textareas in rule edit page for this module.


Block Integration:
==================
The module defines one block named 'JS/CSS Insert info' which can be enabled at:
'admin/build/blocks', the block lists rules loaded in the current page and the
quick-edit feature is built into the block output using a Full-screen semi-Ajax
interface layout.
