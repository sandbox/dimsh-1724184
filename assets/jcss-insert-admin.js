Drupal.jcss_insert = Drupal.jcss_insert || {};

(function ($) {

  Drupal.jcss_insert.type_select_changed = function() {
    if($('.jcss-insert-type').length) {
      if($('.jcss-insert-type').val() == Drupal.settings.jcss_insert.rule_type_js) {
        $('#jcss-insert-css-normal-container, #jcss-insert-css-rtl-normal-container, #jcss-insert-rtl-standalone-container, .jcss-insert-media').hide();
        $('#jcss-insert-js-normal-container, .jcss-insert-scope, .jcss-insert-inline').show();
      }
      else if($('.jcss-insert-type').val() == Drupal.settings.jcss_insert.rule_type_css) {
        $('#jcss-insert-css-normal-container, #jcss-insert-css-rtl-normal-container, #jcss-insert-rtl-standalone-container, .jcss-insert-media').show();
        $('#jcss-insert-js-normal-container, .jcss-insert-scope, .jcss-insert-inline').hide();
      }
    }
  }

  Drupal.jcss_insert.to_full = function() {
    var $jcss = Drupal.jcss_insert;

    // save the current height of the text field to restore it later.
    $.extend(Drupal.settings.jcss_insert, {
      js_height      : $jcss.js_editarea_wrapper('textarea').css('height'),
      css_height     : $jcss.css_editarea_wrapper('textarea').css('height'),
      css_rtl_height : $jcss.css_rtl_editarea_wrapper('textarea').css('height'),

      js_width       : $jcss.js_editarea_wrapper('textarea').css('width'),
      css_width      : $jcss.css_editarea_wrapper('textarea').css('width'),
      css_rtl_width  : $jcss.css_rtl_editarea_wrapper('textarea').css('width')
    });


    // show the overlay div, change the parent for the edit wrappers, and remove a class which adds extra margin.
    $jcss.full_container().show();
    $jcss.full_innerhtml()
      .append($jcss.js_editarea_wrapper().removeClass('form-item'))
      .append($jcss.css_editarea_wrapper().removeClass('form-item'))
      .append($jcss.css_rtl_editarea_wrapper().removeClass('form-item'))
      .append($jcss.save_continue_button().removeClass('form-submit'));

    var $el = $jcss.js_editarea_wrapper().is(':visible') ? $jcss.js_editarea_wrapper() : $jcss.css_editarea_wrapper();
    var $extra_height = 0;
    do {
      $extra_height += $jcss.adjacent_elements_height($el);
      $el = $el.parent();
    } while (!$el.jcss_insert_equals($jcss.full_container()));
    var $height = ($jcss.full_container().height() - $extra_height) + "px";

    // adjust the height of the textarea element.
    // hide the label and description of textarea.
    if($jcss.js_editarea_wrapper().length) {
      $el = $jcss.js_editarea_wrapper('textarea');
      $el.css('height', $height);
      $el.css('width', '100%');
      $jcss.js_editarea_wrapper('label, .description').hide();
    }
    if($jcss.css_editarea_wrapper().length) {
      $el = $jcss.css_editarea_wrapper('textarea');
      $el.css('height', $height);
      $el.css('width', '100%');
      $jcss.css_editarea_wrapper('label, .description').hide();

    }
    if($jcss.css_rtl_editarea_wrapper().length) {
      $el = $jcss.css_rtl_editarea_wrapper('textarea');
      $el.css('height', $height);
      $el.css('width', '100%');
      $jcss.css_rtl_editarea_wrapper('label, .description').hide();
    }
  }

  Drupal.jcss_insert.to_normal = function() {
    var $jcss = Drupal.jcss_insert;

   // show the label and description of textarea.
    $jcss.js_editarea_wrapper('label, .description').show();
    $jcss.css_editarea_wrapper('label, .description').show();
    $jcss.css_rtl_editarea_wrapper('label, .description').show();

    $jcss.js_container().append($jcss.js_editarea_wrapper().addClass('form-item').show());
    $jcss.css_container().append($jcss.css_editarea_wrapper().addClass('form-item').show());
    $jcss.css_rtl_container().append($jcss.css_rtl_editarea_wrapper().addClass('form-item').show());

    var $cur;
    $cur = Drupal.settings.jcss_insert.js_height && $jcss.js_editarea_wrapper('textarea').css('height', $cur);
    $cur = Drupal.settings.jcss_insert.css_height && $jcss.css_editarea_wrapper('textarea').css('height', $cur);
    $cur = Drupal.settings.jcss_insert.css_rtl_height && $jcss.css_rtl_editarea_wrapper('textarea').css('height', $cur);

    $cur = Drupal.settings.jcss_insert.js_width && $jcss.js_editarea_wrapper('textarea').css('width', $cur);
    $cur = Drupal.settings.jcss_insert.css_width && $jcss.css_editarea_wrapper('textarea').css('width', $cur);
    $cur = Drupal.settings.jcss_insert.css_rtl_width && $jcss.css_rtl_editarea_wrapper('textarea').css('width', $cur);


    $jcss.full_container().hide();
    $jcss.save_continue_placeholder().append($jcss.save_continue_button().addClass('form-submit'));
  }

  Drupal.behaviors.jcss_insert_admin = function(context) {
    var $jcss = Drupal.jcss_insert;
    if($('body:not(.jcss-insert-admin-processed)', context).length != 0) {
      $('body:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed');

      // call the function when the first page load.
      $jcss.type_select_changed();
    }

    $('.jcss-insert-js-normal-to-full:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed').each(function() {
      $(this).click(function() {
        $jcss.css_tab().hide();
        $jcss.css_rtl_tab().hide();
        $jcss.js_editarea_wrapper().show();
        $jcss.css_editarea_wrapper().hide();
        $jcss.css_rtl_editarea_wrapper().hide();

        $jcss.to_full();
        return false;
      });
    });

    $('.jcss-insert-css-normal-to-full:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed').each(function() {
      $(this).click(function() {
        $jcss.js_editarea_wrapper().hide();
        $jcss.css_editarea_wrapper().show();
        $jcss.css_rtl_editarea_wrapper().show();

        $jcss.tabs_show();
        $jcss.to_full();
        return false;
      });
    });

    $('.jcss-insert-css-rtl-normal-to-full:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed').each(function() {
      $(this).click(function() {
        $jcss.js_editarea_wrapper().hide();
        $jcss.css_editarea_wrapper().show();
        $jcss.css_rtl_editarea_wrapper().show();

        $jcss.tabs_show('rtl');
        $jcss.to_full();
        return false;
      });
    });

    $('.jcss-insert-full-to-normal:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed').each(function() {
      $(this).click(function() {
        $jcss.to_normal();
        return false;
      });
    });

    $('#jcss-insert-css-tab:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed').click(function() {
      $jcss.tabs_show();
      return false;
    });

    $('#jcss-insert-css-rtl-tab:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed').click(function() {
      $jcss.tabs_show('rtl');
      return false;
    });

    $('#.jcss-insert-type:not(.jcss-insert-admin-processed)', context).addClass('jcss-insert-admin-processed').change(function() {
      $jcss.type_select_changed();
      return false;
    });

  }

})(jQuery);
