Drupal.jcss_insert = Drupal.jcss_insert || {};

(function ($) {

  /**
   * Check equality of two jQuery objects.
   */
  $.fn.jcss_insert_equals = function(compareTo) {
    if (!compareTo || this.length != compareTo.length) {
      return false;
    }
    for (var i = 0; i < this.length; ++i) {
      if (this[i] !== compareTo[i]) {
        return false;
      }
    }
    return true;
  };

  $.extend(Drupal.jcss_insert, {
    tabs        : function() { return $('#jcss-insert-tabs'); },
    css_tab     : function() { return $('#jcss-insert-css-tab'); },
    css_rtl_tab : function() { return $('#jcss-insert-css-rtl-tab'); },

    quick_edit_form : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-admin-quick-edit')) : $('#jcss-insert-admin-quick-edit'); },

    js_editarea_wrapper      : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-js-editarea-wrapper')) : $('#jcss-insert-js-editarea-wrapper'); },
    css_editarea_wrapper     : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-css-editarea-wrapper')) : $('#jcss-insert-css-editarea-wrapper'); },
    css_rtl_editarea_wrapper : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-css-rtl-editarea-wrapper')) : $('#jcss-insert-css-rtl-editarea-wrapper'); },

    js_container      : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-js-normal-container')) : $('#jcss-insert-js-normal-container'); },
    css_container     : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-css-normal-container')) : $('#jcss-insert-css-normal-container'); },
    css_rtl_container : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-css-rtl-normal-container')) : $('#jcss-insert-css-rtl-normal-container'); },

    full_container : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-full-container')) : $('#jcss-insert-full-container'); },
    full_innerhtml : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-full-innerhtml')) : $('#jcss-insert-full-innerhtml'); },

    save_continue_button      : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-save-continue-button')) : $('#jcss-insert-save-continue-button'); },
    save_continue_placeholder : function(selector) { return (typeof selector != "undefined" && selector) ? $(selector, $('#jcss-insert-save-continue-button-placeholder')) : $('#jcss-insert-save-continue-button-placeholder'); },

    adjacent_elements_height : function($el) {
      var $extra_height = 0;
      $('> *', $el.parent()).each(function () {
        if($(this).is(':visible') && !$(this).jcss_insert_equals($el)) {
          $extra_height += parseInt($(this).outerHeight(true));
        }
      });
      return $extra_height;
    },

    tabs_show : function($what) {
      this.css_tab().show();
      this.css_rtl_tab().show();

      if (typeof $what != "undefined" && $what == 'rtl') {
        this.css_editarea_wrapper().hide();
        this.css_rtl_editarea_wrapper().show();

        this.css_tab().removeClass('active');
        this.css_rtl_tab().addClass('active');
      }
      else {
        this.css_editarea_wrapper().show();
        this.css_rtl_editarea_wrapper().hide();

        this.css_tab().addClass('active');
        this.css_rtl_tab().removeClass('active');
      }
    },

    /**
     * From http://stackoverflow.com/a/3373056
     */
    getInputSelection : function(el) {
      var start = 0, end = 0, normalizedValue, range,
          textInputRange, len, endRange;

      if (typeof el.selectionStart == "number" && typeof el.selectionEnd == "number") {
        start = el.selectionStart;
        end = el.selectionEnd;
      } else {
        range = document.selection.createRange();

        if (range && range.parentElement() == el) {
          len = el.value.length;
          normalizedValue = el.value.replace(/\r\n/g, "\n");

          // Create a working TextRange that lives only in the input
          textInputRange = el.createTextRange();
          textInputRange.moveToBookmark(range.getBookmark());

          // Check if the start and end of the selection are at the very end
          // of the input, since moveStart/moveEnd doesn't return what we want
          // in those cases
          endRange = el.createTextRange();
          endRange.collapse(false);

          if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
            start = end = len;
          } else {
            start = -textInputRange.moveStart("character", -len);
            start += normalizedValue.slice(0, start).split("\n").length - 1;

            if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
              end = len;
            } else {
              end = -textInputRange.moveEnd("character", -len);
              end += normalizedValue.slice(0, end).split("\n").length - 1;
            }
          }
        }
      }

      return {
        start: start,
        end: end
      };
    },

    /**
     * From: http://blog.vishalon.net/index.php/javascript-getting-and-setting-caret-position-in-textarea/
     */
    setSelection : function(el, selection) {
      if(el.setSelectionRange) {
        el.focus();
        el.setSelectionRange(selection.start, selection.end);
      }
      else if (el.createTextRange) {
        var range = el.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selection.start);
        range.moveStart('character', selection.end);
        range.select();
      }
    },

    handle_tab_key : function($jq_object) {
      var $sel = this.getInputSelection($jq_object[0]);
      var $val = $jq_object.val();

      if($sel.start == $sel.end) {
        // symply insert tab spaces.
        $jq_object.val($val.substr(0, $sel.start) + "  " + $val.substr($sel.end, $val.length - 1));
        $sel.start += 2;
        $sel.end   += 2;
        this.setSelection($jq_object[0], $sel);
        return;
      }

      var $orig_sel = { start: $sel.start, end: $sel.end };

      if($sel.start > 0 && $val.substr($sel.start, 1) != "\n") {
        var part = $val.substr(0, $sel.start);
        var part_nl_pos = part.split("").reverse().join("").indexOf("\n");
        if(part_nl_pos != -1) {
          $sel.start = part.length - part_nl_pos;
        }
      }

      if($sel.end < $val.length - 1 && $val.substr($sel.end, 1) != "\n") {
        var part_nl_pos = $val.substr($sel.end, $val.length - 1).indexOf("\n");
        if(part_nl_pos != -1) {
          $sel.end = $sel.end + part_nl_pos;
        }
      }
      $mid_count_nl = $val.substr($sel.start, $sel.end - $sel.start).split("\n").length - 1;

      $jq_object.val(
          $val.substr(0, $sel.start) + "  " +
          $val.substr($sel.start, $sel.end - $sel.start).replace(/\n/g, "\n  ") +
          $val.substr($sel.end, $val.length - 1)
      );

      $orig_sel.start += 2;
      $orig_sel.end += 2 * ($mid_count_nl + 1);
      this.setSelection($jq_object[0], $orig_sel);
      return;
    },

    keydown_handler : function (event, $jq_el) {
      var TABKEY = 9;
      if(event.which == TABKEY) {
        event.preventDefault();
        $jq_el.focus();
        if(!event.shiftKey) {
          this.handle_tab_key($jq_el);
        }
      }
    },

    attach_keydown : function(context) {
      if(this.js_editarea_wrapper().length) {
        this.js_editarea_wrapper('textarea').keydown(function(event) {
          return Drupal.jcss_insert.keydown_handler(event, $(this));
        });
      }

      if(this.css_editarea_wrapper().length) {
        this.css_editarea_wrapper('textarea').keydown(function(event) {
          return Drupal.jcss_insert.keydown_handler(event, $(this));
        });
      }

      if(this.css_rtl_editarea_wrapper().length) {
        this.css_rtl_editarea_wrapper('textarea').keydown(function(event) {
          return Drupal.jcss_insert.keydown_handler(event, $(this));
        });
      }
    }
  });

  Drupal.behaviors.jcss_insert_common = function(context) {
    if($('body:not(.jcss-insert-common-processed)', context).length == 0) {
      $('body:not(.jcss-insert-common-processed)', context).addClass('jcss-insert-common-processed');
      Drupal.jcss_insert.attach_keydown();
    }
  }

})(jQuery);
