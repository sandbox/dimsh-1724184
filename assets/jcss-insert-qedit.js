Drupal.jcss_insert = Drupal.jcss_insert || {};

(function ($) {

Drupal.jcss_insert.adjust_height = function() {
  var $jcss = Drupal.jcss_insert;
  if($jcss.quick_edit_form().length) {
    var $el = $jcss.js_editarea_wrapper().is(':visible') ? $jcss.js_editarea_wrapper() : $jcss.css_editarea_wrapper();
    var $extra_height = 0;
    do {
      $extra_height += $jcss.adjacent_elements_height($el);
      $el = $el.parent();
    } while (!$el.jcss_insert_equals($jcss.full_container()));
    var $height = ($jcss.full_container().height() - $extra_height) + "px";

    $jcss.quick_edit_form('textarea').css('height', $height);
    $jcss.quick_edit_form('textarea').css('width', '98%');
  }
}

Drupal.behaviors.jcss_insert_quick = function(context) {
  var $jcss = Drupal.jcss_insert;
  if($('body:not(.jcss-insert-quick-processed)', context).length != 0) {
    $('body:not(.jcss-insert-quick-processed)', context).addClass('jcss-insert-quick-processed');

    if($jcss.full_container().length == 0) {
      $('<div id="jcss-insert-full-container">' +
          '<a class="jcss-insert-full-to-normal" href="#">' + Drupal.t('Close full screen') + '</a>' +
          '<div id="jcss-insert-tabs">' +
            '<a id="jcss-insert-css-tab" href="#">' + Drupal.t('CSS code') + '</a>' +
            '<a id="jcss-insert-css-rtl-tab" href="#">' + Drupal.t('RTL CSS code') + '</a>' +
          '</div>' +
          '<div id="jcss-insert-full-innerhtml"></div>' +
        '</div>').appendTo($('body', context));
    }
    $jcss.full_container().hide();

    $('.jcss-insert-block-item').each(function () {
      var $el = $(this);
      $el.click(function () {
        var classes = $el.attr('class').split(' ');
        var url = '';
        for(var i = 0; i < classes.length; i++) {
          if(classes[i].indexOf('jcss-insert-qlink-') == 0) {
            url = Drupal.settings.jcss_insert.qedit_url_prifix + '/' + classes[i].replace('jcss-insert-qlink-', '');
            break;
          }
        }

        if(url != '') {
          $.get(url, 'jcss-from=' + encodeURIComponent(window.location.href), function(data) {
            $jcss.full_innerhtml().html(data);
            $jcss.full_container().show();
            $jcss.tabs_show();
            $jcss.adjust_height();

            if($jcss.css_editarea_wrapper().length == 0 || $jcss.css_rtl_editarea_wrapper().length == 0) {
              // hide the tabs if no CSS wrappers are defined, since we are editing a JS code.
              $jcss.css_tab().hide();
              $jcss.css_rtl_tab().hide();
            }

            $jcss.js_editarea_wrapper().removeClass('form-item');
            $jcss.css_editarea_wrapper().removeClass('form-item');
            $jcss.css_rtl_editarea_wrapper().removeClass('form-item');

            Drupal.jcss_insert.attach_keydown();
          });
          return false;
        }
        return true;
      });
    });

  }

  $('.jcss-insert-full-to-normal:not(.jcss-insert-quick-processed)', context).addClass('jcss-insert-quick-processed').each(function() {
    $(this).click(function() {
      Drupal.jcss_insert.full_container().hide();
      return false;
    });
  });

  $('#jcss-insert-css-tab:not(.jcss-insert-quick-processed)', context).addClass('jcss-insert-quick-processed').click(function() {
    Drupal.jcss_insert.tabs_show();
    return false;
  });

  $('#jcss-insert-css-rtl-tab:not(.jcss-insert-quick-processed)', context).addClass('jcss-insert-quick-processed').click(function() {
    Drupal.jcss_insert.tabs_show('rtl');
    return false;
  });

}

})(jQuery);
