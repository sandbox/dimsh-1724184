<?php
/**
 * @file
 * JS/CSS Insert module admin functions file.
 */

/**
 * Form constructor for JS/CSS Insert overview form.
 */
function jcss_insert_admin_form(&$form_state) {
  $path = jcss_insert()->modulePath . '/assets/';

  $form = array('#tree' => TRUE);

  foreach (jcss_insert()->ruleObjectsGetAll(NULL, TRUE) as $rule_object) {
    $form['rules'][$rule_object->crid]['rule'] = array(
      '#type' => 'value',
      '#value' => $rule_object
    );

    $form['rules'][$rule_object->crid]['edit'] = array(
      '#type' => 'image_button',
      '#title' => t('Edit rule'),
      '#src' => $path . 'text-editor.png',
      '#submit' => array('jcss_insert_admin_edit_button'),
      '#crid' => $rule_object->crid,
    );
    $form['rules'][$rule_object->crid]['delete'] = array(
      '#type' => 'image_button',
      '#title' => t('Delete rule'),
      '#src' => $path . 'edit-delete.png',
      '#submit' => array('jcss_insert_admin_delete_button'),
      '#crid' => $rule_object->crid,
    );
    $form['rules'][$rule_object->crid]['enabled'] = array(
      '#type'           => 'checkbox',
      '#default_value'  => !$rule_object->disabled,
    );
    $form['rules'][$rule_object->crid]['weight'] = array(
      '#type'           => 'weight',
      '#delta'          => 50,
      '#default_value'  => !$rule_object->weight,
      '#attributes'     => array('class' => 'rule-weight'),
    );
  }

  $form['module_settings'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Module settings'),
    '#collapsible'    => TRUE,
    '#collapsed'      => FALSE,

    'jcss_insert_check_method' => array(
      '#type'           => 'radios',
      '#title'          => t('Check method for file content updates'),
      '#options'        => array(
        'none'       => t('None'),
        'mtime'      => t('Modification time [mtime]'),
        'mtime_size' => t('File size and modification time [mtime]'),
        'md5'        => t('MD5 checksum'),
      ),
      '#default_value'  => variable_get('jcss_insert_check_method', 'none'),
      '#description'    => t('The method used to check that CSS/JS files contents are accurate and in sync with CSS data stored in the database.<br />Generally, there is no need for the check because the module ensures that data get written in files as soon as the rule which holds them is saved, but this option is given here for any reason which will make cause the data in the database be modified by ways out of control of this module save handler.'),
    ),

    'jcss_insert_mtime_diff' => array(
      '#type'           => 'select',
      '#title'          => t('Modification time allowed seconds'),
      '#options'        => drupal_map_assoc(range(1, 10)),
      '#default_value'  => variable_get('jcss_insert_mtime_diff', 2),
      '#description'    => t('If the choosen <strong>Check method for file content updates</strong> is <strong>modification time</strong> related, this will be the allowed difference in seconds between database record modification time and the corresponding file modification time.'),
    ),
  );

  $form['buttons']['save'] = array(
    '#type'    => 'submit',
    '#value'   => t('Save'),
    '#submit'  => array('jcss_insert_admin_form_save'),
  );
  $form['buttons']['restore'] = array(
    '#type'    => 'submit',
    '#value'   => t('Restore defaults'),
    '#submit'  => array('jcss_insert_admin_form_restore'),
  );

  return $form;
}

/**
 * Form submission handler for jcss_insert_admin_form().
 *
 * Used for Save action.
 */
function jcss_insert_admin_form_save($form, &$form_state) {
  variable_set('jcss_insert_check_method', $form_state['values']['module_settings']['jcss_insert_check_method']);
  variable_set('jcss_insert_mtime_diff', $form_state['values']['module_settings']['jcss_insert_mtime_diff']);

  $success = TRUE;
  foreach (element_children($form_state['values']['rules']) as $crid) {
    $array = $form_state['values']['rules'][$crid];
    $rule_object = $array['rule'];
    if ($rule_object->disabled == $array['enabled'] || $rule_object->weight != $array['weight']) {
      $rule_object->disabled = !$array['enabled'];
      $rule_object->weight   = $array['weight'];
      if (!jcss_insert()->ruleSave($rule_object)) {
        $success = FALSE;
      }
    }
  }
  cache_clear_all();
  if ($success) {
    drupal_set_message(t('Configuration options saved.'));
  }
  else {
    drupal_set_message(t('Error occured while saving configuration.'), 'error');
  }
}

/**
 * Form submission handler for jcss_insert_admin_form().
 *
 * Used for Restore Defaults action.
 */
function jcss_insert_admin_form_restore($form, &$form_state) {
  variable_set('jcss_insert_check_method', 'none');
  variable_set('jcss_insert_mtime_diff', 2);
  cache_clear_all();
  drupal_set_message(t('Configuration options restored'));
}

/**
 * Form submission handler for jcss_insert_admin_form().
 *
 * Used for Rule-Edit action.
 */
function jcss_insert_admin_edit_button($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/jcss-insert/edit/' . $form_state['clicked_button']['#crid'];
}

/**
 * Form submission handler for jcss_insert_admin_form().
 *
 * Used for Rule-Delete action.
 */
function jcss_insert_admin_delete_button($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/jcss-insert/delete/' . $form_state['clicked_button']['#crid'];
}

/**
 * Theme function for the admin overview form.
 */
function theme_jcss_insert_admin_form($form) {
  drupal_add_tabledrag('jcss-insert-admin-table', 'order', 'sibling', 'rule-weight');

  $headers = array(
    t('Enabled'),
    t('Weight'),
    t('Title'),
    t('Actions'),
    t('Type'),
    t('Conditional insertion'),
    t('Insert as'),
    t('Uses PHP'),
  );
  $rows = array();
  if (!empty($form['rules'])) {
    foreach (element_children($form['rules']) as $crid) {
      $row = array();
      $rule_object = $form['rules'][$crid]['rule']['#value'];

      $row[] = drupal_render($form['rules'][$crid]['enabled']);
      $row[] = drupal_render($form['rules'][$crid]['weight']);
      $row[] = check_plain($rule_object->title);
      $row[] = drupal_render($form['rules'][$crid]['edit']) . drupal_render($form['rules'][$crid]['delete']);
      $row[] = $rule_object->type == JcssInsert::RULE_TYPE_CSS ? t('CSS') : t('JS');
      $row[] = $rule_object->pages ? t('Yes') : t('No');
      $row[] = $rule_object->as_theme ? t('Theme') : t('Module');
      $row[] = $rule_object->eval_php ? t('Yes') : t('No');

      $rows[] = array('data' => $row, 'class' => 'draggable');
    }
  }

  $link = l(t('Create a new rule'), 'admin/settings/jcss-insert/add');
  if (empty($rows)) {
    $rows[] = array(array(
        'data' => t('No CSS insertion rules have been set up yet.'),
        'colspan' => count($headers),
    ));
  }

  $rows[] = array(array(
      'data' => $link,
      'colspan' => count($headers),
  ));

  $output .= theme('table', $headers, $rows, array('id' => 'jcss-insert-admin-table'));
  $output .= drupal_render($form);
  return $output;
}

/**
 * Helper used to get a FORM markup result and process it if submitted.
 *
 * Used for custom implementation of quick edit feature without AJAX form
 * submission.
 */
function jcss_insert_admin_get_form() {
  $args = func_get_args();
  print call_user_func_array('drupal_get_form', $args);
  exit(0);
}

/**
 * Access callback for the quick edit form.
 */
function jcss_insert_admin_qedit_access($crid) {
  if (is_numeric($crid) && $crid) {
    $rule_object = jcss_insert()->ruleObjectsGetAll($crid);
    $rule_object = $rule_object[$crid];
    if (!is_object($rule_object)) {
      return FALSE;
    }
  }
  else {
    return FALSE;
  }

  return $rule_object->eval_php ? (user_access('administer jcss insert') && user_access('use PHP evaluate for rule code')) : user_access('administer jcss insert');
}

/**
 * Form constructor for the quick edit form.
 *
 * @param $crid
 *   The rule primary-key.
 */
function jcss_insert_admin_quick_form(&$form_state, $crid) {
  if (is_numeric($crid) && $crid) {
    $rule_object = jcss_insert()->ruleObjectsGetAll($crid);
    $rule_object = $rule_object[$crid];
    if (!is_object($rule_object)) {
      drupal_not_found();
      exit();
    }
  }
  else {
    drupal_not_found();
    exit();
  }

  $form = array(
    '#id'         => 'jcss-insert-admin-quick-edit',
    'rule_object' => array(
      '#type'  => 'value',
      '#value' => $rule_object,
    ),
  );

  if ($rule_object->type == JcssInsert::RULE_TYPE_CSS) {
    $form['css'] = array(
      '#prefix'         => '<div id="jcss-insert-css-normal-container">',
      '#suffix'         => '</div>',
      '#type'           => 'textarea',
      '#title'          => '',
      '#rows'           => 10,
      '#default_value'  => $rule_object->css,
      '#id'             => 'jcss-insert-css-editarea',
      '#attributes'     => array('class' => 'jcss-insert-code'),
    );

    $form['css_rtl'] = array(
      '#prefix'         => '<div id="jcss-insert-css-rtl-normal-container">',
      '#suffix'         => '</div>',
      '#type'           => 'textarea',
      '#title'          => '',
      '#rows'           => 10,
      '#default_value'  => $rule_object->css_rtl,
      '#id'             => 'jcss-insert-css-rtl-editarea',
      '#attributes'     => array('class' => 'jcss-insert-code'),
    );
  }

  if ($rule_object->type == JcssInsert::RULE_TYPE_JS) {
    $form['js'] = array(
      '#prefix'         => '<div id="jcss-insert-js-normal-container">',
      '#suffix'         => '</div>',
      '#type'           => 'textarea',
      '#title'          => '',
      '#rows'           => 10,
      '#default_value'  => $rule_object->js,
      '#id'             => 'jcss-insert-js-editarea',
      '#attributes'     => array('class' => 'jcss-insert-code'),
    );
  }

  $form['save'] = array(
    '#type'    => 'submit',
    '#value'   => t('Save'),
    '#id'      => 'jcss-insert-admin-quick-edit-submit',
  );

  return $form;
}

/**
 * Form submission handler for jcss_insert_admin_quick_form().
 */
function jcss_insert_admin_quick_form_submit($form, &$form_state) {
  $v = $form_state['values'];
  $rule_object = $v['rule_object'];

  if (is_object($rule_object)) {
    if ($rule_object->type == JcssInsert::RULE_TYPE_CSS) {
      $rule_object->css = $v['css'];
      $rule_object->css_rtl = $v['css_rtl'];
    }
    if ($rule_object->type == JcssInsert::RULE_TYPE_JS) {
      $rule_object->js = $v['js'];
    }
    if (jcss_insert()->ruleSave($rule_object)) {
      drupal_set_message(t('Rule %title edited successfully.', array('%title' => $rule_object->title)));
    }
    else {
      drupal_set_message(t('Failed to edit rule %title.', array('%title' => $rule_object->title)), 'error');
    }
  }

  if ($_GET['jcss-from'] && strpos($_GET['jcss-from'], $GLOBALS['base_url']) === 0) {
    // Will not redirect to foreign URLs.
    $form_state['redirect'] = $_GET['jcss-from'];
  }
  else {
    $form_state['redirect'] = 'admin/settings/jcss-insert/edit/' . $rule_object->crid;
  }
}

/**
 * Form constructor for rule edit or add form.
 */
function jcss_insert_admin_edit(&$form_state, $crid = NULL) {
  $form = array();

  if (isset($crid)) {
    if (is_numeric($crid) && $crid) {
      $rule_object = jcss_insert()->ruleObjectsGetAll($crid);
      $rule_object = $rule_object[$crid];
      if (!is_object($rule_object)) {
        drupal_not_found();
        exit();
      }
      $form['rule_object'] = array(
        '#type'  => 'value',
        '#value' => $rule_object,
      );
    }
    else {
      drupal_not_found();
      exit();
    }
  }
  else {
    $rule_object = new stdClass();
    $rule_object->disabled        = 0;
    $rule_object->type            = JcssInsert::RULE_TYPE_CSS;
    $rule_object->title           = '';
    $rule_object->description     = '';
    $rule_object->visibility      = 0;
    $rule_object->pages           = '';
    $rule_object->media           = 'all';
    $rule_object->preprocess      = 1;
    $rule_object->as_theme        = 0;
    $rule_object->css             = '';
    $rule_object->css_rtl         = '';
    $rule_object->rtl_standalone  = 0;
    $rule_object->js              = '';
    $rule_object->scope           = 'header';
    $rule_object->inline          = 0;
    $rule_object->eval_php        = 0;
    $rule_object->report_errors   = 0;
    $rule_object->weight          = 0;
  }

  jcss_insert()->blockDisabled(TRUE);
  drupal_add_css(jcss_insert()->modulePath . '/assets/jcss-insert.css', 'module');
  drupal_add_js(jcss_insert()->modulePath . '/assets/jcss-insert-common.js', 'module');
  drupal_add_js(jcss_insert()->modulePath . '/assets/jcss-insert-admin.js', 'module');
  drupal_add_js(array(
    'jcss_insert' => array(
      'js_height'       => 0,
      'css_height'      => 0,
      'css_rtl_height'  => 0,
      'js_width'        => 0,
      'css_width'       => 0,
      'css_rtl_width'   => 0,
      'rule_type_js'    => JcssInsert::RULE_TYPE_JS,
      'rule_type_css'   => JcssInsert::RULE_TYPE_CSS,
    ),
  ), 'setting');


  $form['disabled'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Disabled rule'),
    '#default_value'  => $rule_object->disabled,
    '#description'    => t('To disable loading this rule keeping definitions, check this.'),
  );

  $form['type'] = array(
    '#type'           => 'select',
    '#title'          => t('Rule type'),
    '#options'        => array(JcssInsert::RULE_TYPE_CSS => t('CSS'), JcssInsert::RULE_TYPE_JS => t('Javascript')),
    '#default_value'  => $rule_object->type,
    '#description'    => t('Rule type so provided code is CSS or Javascript.'),
    '#attributes'      => array('class' => 'jcss-insert-type'),
  );

  $form['title'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Title'),
    '#default_value'  => $rule_object->title,
    '#required'       => TRUE,
  );

  $form['description'] = array(
    '#type'           => 'textarea',
    '#title'          => t('Description'),
    '#rows'           => 4,
    '#default_value'  => $rule_object->description,
    '#description'    => t('The description or comments about this rule.'),
  );

  $form['as_theme'] = array(
    '#type'           => 'select',
    '#title'          => t('Load as'),
    '#options'        => array(t('Module CSS'), t('Theme CSS')),
    '#default_value'  => $rule_object->as_theme,
    '#description'    => t('CSS/JS files inserted as belong to a module get loaded before ones inserted as belong to a theme, themes CSS/JS override modules CSS/JS by default in Drupal.<br />If selected to load this rule as a theme, the CSS/JS files will be loaded <strong>AFTER</strong> all modules and themes CSS/JS files, this is an added value of this module by which it loads CSS/JS files marked as Theme after all CSS/JS files loaded by the active theme itself, allowing developers to override CSS classes definition or JS variables.'),
  );

  $form['full_container'] = array(
    '#type'   => 'markup',
    '#value'  => '<div id="jcss-insert-full-container">
                    <a class="jcss-insert-full-to-normal" href="#">' . t('Close full screen') . '</a>
                    <div id="jcss-insert-tabs">
                      <a id="jcss-insert-css-tab" href="#">' . t('CSS code') . '</a>
                      <a id="jcss-insert-css-rtl-tab" href="#">' . t('RTL CSS code') . '</a>
                    </div>
                    <div id="jcss-insert-full-innerhtml"></div>
                  </div>',
  );

  $full_screen = t('edit in full screen');

  $css_edit_link = '<a class="jcss-insert-css-normal-to-full" href="#">' . $full_screen . '</a>';
  $form['css'] = array(
    '#prefix'         => '<div id="jcss-insert-css-normal-container">',
    '#suffix'         => '</div>',
    '#type'           => 'textarea',
    '#title'          => t('CSS code - !edit_link', array('!edit_link' => $css_edit_link)),
    '#rows'           => 10,
    '#default_value'  => $rule_object->css,
    '#description'    => t('The CSS definition for this rule, requried if <strong>Stanalone RTL</strong> options is NOT checked or checked and no <strong>RTL CSS code</strong> supplied. - !edit_link', array('!edit_link' => $css_edit_link)),
    '#id'             => 'jcss-insert-css-editarea',
    '#attributes'      => array('class' => 'jcss-insert-code'),
  );

  $css_rtl_edit_link = '<a class="jcss-insert-css-rtl-normal-to-full" href="#">' . $full_screen . '</a>';
  $form['css_rtl'] = array(
    '#prefix'         => '<div id="jcss-insert-css-rtl-normal-container">',
    '#suffix'         => '</div>',
    '#type'           => 'textarea',
    '#title'          => t('RTL CSS code - !edit_link', array('!edit_link' => $css_rtl_edit_link)),
    '#rows'           => 10,
    '#default_value'  => $rule_object->css_rtl,
    '#description'    => t('The RTL languages CSS definition for this rule. - !edit_link', array('!edit_link' => $css_rtl_edit_link)),
    '#id'             => 'jcss-insert-css-rtl-editarea',
    '#attributes'      => array('class' => 'jcss-insert-code'),
  );

  $form['rtl_standalone'] = array(
    '#prefix'         => '<div id="jcss-insert-rtl-standalone-container">',
    '#suffix'         => '</div>',
    '#type'           => 'checkbox',
    '#title'          => t('Stanalone RTL'),
    '#default_value'  => $rule_object->rtl_standalone,
    '#description'    => t('If NOT checked and you have supplied RTL CSS, then RTL CSS will be inserted on pages with RTL languages as an override to the normal CSS supplied, this is the default Drupal behaviour and the recommended option.<br />If the option is checked, then the RTL CSS will only be inserted on RTL pages and the normal CSS will <strong>NOT</strong> be inserted on those pages, in this case, you are creating two seperate CSS versions, for LTR and RTL pages.'),
  );

  $js_edit_link = '<a class="jcss-insert-js-normal-to-full" href="#">' . $full_screen . '</a>';
  $form['js'] = array(
    '#prefix'         => '<div id="jcss-insert-js-normal-container">',
    '#suffix'         => '</div>',
    '#type'           => 'textarea',
    '#title'          => t('JS code - !edit_link', array('!edit_link' => $js_edit_link)),
    '#rows'           => 10,
    '#default_value'  => $rule_object->js,
    '#description'    => t('The Javascript definition for this rule. - !edit_link', array('!edit_link' => $js_edit_link)),
    '#id'             => 'jcss-insert-js-editarea',
    '#attributes'      => array('class' => 'jcss-insert-code'),
  );


  $form['conditional'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Custom visibility settings'),
    '#collapsible'    => TRUE,
    '#collapsed'      => $rule_object->pages ? FALSE : TRUE,
  );
  // Taken from block.module.
  $access = user_access('use PHP for rule visibility');
  if ($rule_object->visibility == 2 && !$access) {
    $form['conditional']['visibility'] = array('#type' => 'value', '#value' => 2);
    $form['conditional']['pages'] = array('#type' => 'value', '#value' => $rule_object->pages);
  }
  else {
    $options = array(t('Add on every page except the listed pages.'), t('Add on only the listed pages.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if ($access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' ' . t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['conditional']['visibility'] = array(
      '#type'           => 'radios',
      '#title'          => t('Add the CSS on specific pages'),
      '#options'        => $options,
      '#default_value'  => $rule_object->visibility,
    );
    $form['conditional']['pages'] = array(
      '#type'           => 'textarea',
      '#title'          => t('Pages'),
      '#default_value'  => $rule_object->pages,
      '#description'    => $description,
    );
  }

  $form['media'] = array(
    '#prefix'         => '<div class="jcss-insert-media">',
    '#suffix'         => '</div>',
    '#type'           => 'select',
    '#title'          => t('Media'),
    '#options'        => array(
      'all'     => t('All'),
      'screen'  => t('Screen'),
      'print'   => t('Print'),
    ),
    '#default_value'  => $rule_object->media,
  );

  $form['preprocess'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Preprocess'),
    '#default_value'  => $rule_object->preprocess,
    '#description'    => t('Tell that the preprocess parameter should be TRUE when loading this rule so Drupal CSS/JS preprocessing and aggregation will apply on this rule if activated.'),
  );

  $form['scope'] = array(
    '#prefix'         => '<div class="jcss-insert-scope">',
    '#suffix'         => '</div>',
    '#type'           => 'select',
    '#title'          => t('JS Scope'),
    '#options'        => array('header' => t('Header'), 'footer' => t('Footer')),
    '#default_value'  => $rule_object->scope,
    '#description'    => t('The placement of the JS code can be at the header or footer for the page.'),
  );

  $form['inline'] = array(
    '#prefix'         => '<div class="jcss-insert-inline">',
    '#suffix'         => '</div>',
    '#type'           => 'checkbox',
    '#title'          => t('Inline JS'),
    '#default_value'  => $rule_object->inline,
    '#description'    => t('Place this JS code inline without loading it by a file.'),
  );

  $access = user_access('use PHP evaluate for rule code');
  if (!$access) {
    // If a user who is granted 'administer jcss insert' but not 'use PHP
    // evaluate for rule code' is editing a rule previously set to evaluate
    // PHP code, we will disable the evaluation when saving it to prevent leaks.
    $form['eval_php'] = array('#type' => 'value', '#value' => 0);
  }
  else {
    $form['eval_php'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Evaluate PHP in Code'),
      '#default_value'  => $rule_object->eval_php,
      '#description'    => t('If checked, the code provided will be passed to PHP parser before being saved into the related file for includion in the page, this will allow for creaing CSS/JS rules which varies according site conditions or variables. <br />Please note that the implementation uses the OUTPUT of the eval function and not the RETURN value. -- It makes difference but there is no point in taking the return value for this implementation.'),
    );
  }

  $form['report_errors'] = array(
    '#type'           => 'select',
    '#title'          => t('Report inclusion failure'),
    '#options'        => array(
      JcssInsert::PROCESS_ERRORS_REPORT_NO           => t('No reporting'),
      JcssInsert::PROCESS_ERRORS_REPORT_MODULE_ADMIN => t('Only for module admin'),
      JcssInsert::PROCESS_ERRORS_REPORT_YES          => t('Yes, always'),
    ),
    '#default_value'  => $rule_object->report_errors,
    '#description'    => t('If the inclusion of a rule failed because of it data write error, a log message is set to let the admin know, this option will also let a Drupal status message to appear on each page where a data write error occur, this might not be what you want so the option is disabled by default.<br />The <strong>Only for module admin</strong> options causes the status message to only appear to users who have the [administer jcss insert] permission.'),
  );

  $form['weight'] = array(
    '#type'           => 'weight',
    '#title'          => t('Weight'),
    '#delta'          => 50,
    '#default_value'  => $rule_object->weight,
    '#description'    => t('Rules with lower weight load first.'),
  );

  $form['buttons']['save'] = array(
    '#type'    => 'submit',
    '#value'   => t('Save'),
    '#submit'  => array('jcss_insert_admin_edit_save'),
  );
  $form['buttons']['save_and_continue'] = array(
    '#prefix'  => '<span id="jcss-insert-save-continue-button-placeholder">',
    '#suffix'  => '</span>',
    '#type'    => 'submit',
    '#value'   => t('Save and Continue'),
    '#submit'  => array('jcss_insert_admin_edit_save_and_continue'),
    '#id'      => 'jcss-insert-save-continue-button',
  );

  if (!empty($rule_object->crid)) {
    $form['buttons']['delete'] = array(
      '#type'    => 'submit',
      '#value'   => t('Delete'),
      '#submit'  => array('jcss_insert_admin_delete_button'),
      '#crid'    => $rule_object->crid,
    );
  }

  return $form;
}

/**
 * Form submission handler for jcss_insert_admin_edit().
 *
 * Used for Save action.
 */
function jcss_insert_admin_edit_save($form, &$form_state, &$crid = 0) {
  $v = $form_state['values'];
  $rule_object = isset($v['rule_object']) ? $v['rule_object'] : NULL;

  $is_edit = FALSE;
  if ($rule_object) {
    $is_edit = TRUE;
    // Delete old CSS/JS files for the rule being edited.
    jcss_insert()->ruleDeleteFiles($rule_object);
  }
  else {
    $rule_object = new stdClass();
    $rule_object->created = time();
  }

  $success = jcss_insert()->ruleSave($rule_object, $v);

  $vars = array('%title' => $rule_object->title);
  if ($is_edit) {
    if ($success) {
      drupal_set_message(t('Rule %title edited successfully.', $vars));
    }
    else {
      drupal_set_message(t('Failed to edit rule %title.', $vars), 'error');
    }
  }
  else {
    if ($success) {
      drupal_set_message(t('Rule %title created successfully.', $vars));
    }
    else {
      drupal_set_message(t('Failed to create rule %title.', $vars), 'error');
    }
  }

  $form_state['redirect'] = 'admin/settings/jcss-insert';
  $crid = $rule_object->crid;
}

/**
 * Form submission handler for jcss_insert_admin_edit().
 *
 * Used for Save-and-Continue action.
 */
function jcss_insert_admin_edit_save_and_continue($form, &$form_state) {
  jcss_insert_admin_edit_save($form, $form_state, $crid);
  $form_state['redirect'] = 'admin/settings/jcss-insert/edit/' . $crid;
}


/**
 * Form constructor for rule delete form.
 */
function jcss_insert_admin_delete_confirm(&$form_state, $crid = 0) {
  if ($crid) {
    $rule_object = jcss_insert()->ruleObjectsGetAll($crid);
    $rule_object = $rule_object[$crid];
    if (is_object($rule_object)) {
      $form['rule_object'] = array(
        '#type' => 'value',
        '#value' => $rule_object,
      );

      return confirm_form($form,
        t('Are you sure you want to delete rule %title?', array('%title' => $rule_object->title)),
        isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/jcss-insert',
        t('This action cannot be undone.'),
        t('Delete'),
        t('Cancel')
      );
    }
  }
  drupal_not_found();
  exit();
}

/**
 * Form submission handler for jcss_insert_admin_delete_confirm().
 */
function jcss_insert_admin_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    jcss_insert()->ruleRemove($form_state['values']['rule_object']);
    // These two calls will help clear the static cache before page reload in
    // case keep alives did not delete them.
    jcss_insert()->ruleObjectsGetAll(NULL, TRUE);
    jcss_insert()->ruleObjectsGetActive(TRUE);
  }

  $form_state['redirect'] = 'admin/settings/jcss-insert';
}
