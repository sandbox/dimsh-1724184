<?php
/**
 * @file
 * JS/CSS Insert module class definition file.
 *
 * Defines a singleton class which holds helper functions for the module.
 */


/**
 * JS/CSS Insert module singliton class, holds common functions.
 */
class JcssInsert {
  /**
   * Rule types, for {jcss_insert_rules}.type field.
   * @var int
   */
  const RULE_TYPE_CSS = 0;
  const RULE_TYPE_JS  = 1;

  /**
   * Options for rule processing errors reporting through Drupal status message,
   * for {jcss_insert_rules}.report_errors field.
   * @var int
   */
  const PROCESS_ERRORS_REPORT_NO           = 0;
  const PROCESS_ERRORS_REPORT_MODULE_ADMIN = 1;
  const PROCESS_ERRORS_REPORT_YES          = 2;

  /**
   * Singleton instance.
   *
   * @var JcssInsert
   */
  protected static $instance = NULL;

  /**
   * Set in the constructor and point to modules' path.
   * @var string
   */
  public $modulePath;

  /**
   * Variables used in methods of this class to hold values though object life time.
   * @var bool
   */
  protected $drupalFlushCssJsCalled = FALSE;
  protected $blockDisabled               = FALSE;

  /**
   * Get the singleton instance.
   *
   * @return JcssInsert
   */
  public static function getInstance() {
    if (NULL === self::$instance) {
      self::$instance = new JcssInsert();
    }
    return self::$instance;
  }

  /**
   * A private constructor; prevents direct creation of object.
   */
  protected function __construct() {
    // I need this function to unset the instance for Keep-Alive issues.
    register_shutdown_function(array(__CLASS__, 'shutdown'));
    $this->modulePath = dirname(drupal_get_filename('module', 'jcss_insert'));
  }

  /**
   * Prevent users to clone the instance of a singleton.
   */
  public function __clone() {
    trigger_error('Cloning ' . __CLASS__ . ' object is not allowed.', E_USER_ERROR);
  }

  /**
   * Unset the instance, this method is registered as a shutdown function.
   */
  public static function shutdown() {
    if (function_exists('jcss_insert')) {
      jcss_insert(TRUE);
    }
    self::$instance = NULL;
  }

  /**
   * Save file data and create the parent directory if needed.
   *
   * @param string $filename
   * @param string $data
   *
   * @return bool success status
   */
  public function filePutContents($filename, $data) {
    if (!$this->drupalFlushCssJsCalled) {
      // force browsers to get a new copy of CSS/JS files.
      _drupal_flush_css_js();
      $this->drupalFlushCssJsCalled = TRUE;
    }

    $result = (file_check_directory(dirname($filename), FILE_CREATE_DIRECTORY) &&
          (bool) file_save_data($data, $filename, FILE_EXISTS_REPLACE));
    return $result;
  }

  /**
   * Return the directory used to save CSS and JS files before including them.
   */
  public function getSaveDir() {
    return file_directory_path() . '/jcss-insert';
  }

  /**
   * Get an array of all rule objects according to filter provided by $filter
   * parameter.
   *
   * Always return an array.
   *
   * A rule object contains all fields from {jcss_insert_rules} table as
   * properties in addition to properties added by
   * JcssInsert::ruleObjectPrepare().
   *
   * @param $filter
   *   according to the value:
   *   if a numeric value then it is considered a 'crid' primary-key value
   *   if a numeric array with numeric values then they are considered multible
   *   'crid's.
   *   if associative array, then valid keys are:
   *   'crid', 'disabled', 'type', 'as_theme', 'visibility', 'pages', 'eval_php'
   *   all are integers except 'pages' which is checked for being empty if
   *   correspoding value for that key FALSE or being not empty otherwise.
   *
   * @param $reset
   *   reset the static cache.
   *
   * @return
   *   array of stdClass objects.
   */
  public function ruleObjectsGetAll($filter = NULL, $reset = FALSE) {
    static $ret = NULL;
    $cache_key = serialize($filter);

    if (NULL !== $ret && !$reset && isset($ret[$cache_key])) {
      return $ret[$cache_key];
    }

    if (NULL === $ret || $reset) {
      $ret = array();
    }
    $ret[$cache_key] = array();

    $valid_keys = array('crid', 'disabled', 'type', 'as_theme', 'visibility', 'pages', 'eval_php');

    $q = "SELECT * FROM {jcss_insert_rules}";
    $order = " ORDER BY weight ASC";
    $where = $args = array();

    if (isset($filter)) {
      if (is_numeric($filter)) {
        $where[] = "crid = %d";
        $args[] = $filter;
      }
      elseif ($filter && is_array($filter)) {
        if (array_keys($filter) == array_filter(array_keys($filter), 'is_numeric') &&
            $filter == array_filter($filter, 'is_numeric')) {
          $where[] = "crid IN (" . db_placeholders($filter, 'int') . ")";
          $args = $filter;
        }
        else {
          foreach ($filter as $key => $value) {
            if (!in_array($key, $valid_keys)) {
              continue;
            }
            if ($key == 'pages') {
              $where[] = $value ? "pages != ''" : "pages = ''";
            }
            else {
              $where[] = "$key = %d";
              $args[] = $value;
            }
          }
        }
      }
    }

    $result = $where ? db_query($q . " WHERE " . implode(" AND ", $where) . $order, $args) : db_query($q . $order);
    while ($row = db_fetch_object($result)) {
      $this->ruleObjectPrepare($row);
      $ret[$cache_key][$row->crid] = $row;
    }
    return $ret[$cache_key];
  }

  /**
   * Get an array of all rule objects which are active for the current
   * request and those which their CSS or JS definition should be inserted.
   *
   * A rule object contains all fields from {jcss_insert_rules} table as
   * properties in addition to 'file_css', 'file_css_rtl', and 'files'
   * properties.
   *
   * @return
   *   array of objects
   */
  public function ruleObjectsGetActive($reset = FALSE) {
    static $ret = NULL;

    if (NULL !== $ret && !$reset) {
      return $ret;
    }
    $ret = array();

    $is_rtl = $GLOBALS['language']->direction == LANGUAGE_RTL;
    $crids = array();

    $result = db_query("SELECT
      crid, type, pages, visibility,
      js      != '' AS has_js,
      css     != '' AS has_css,
      css_rtl != '' AS has_css_rtl,
      rtl_standalone FROM {jcss_insert_rules} WHERE disabled = 0
      ORDER BY weight ASC");

    while ($row = db_fetch_object($result)) {
      if (($row->type == self::RULE_TYPE_CSS && $is_rtl && $row->rtl_standalone && !$row->has_css_rtl) ||
          ($row->type == self::RULE_TYPE_CSS && !$is_rtl && !$row->has_css) ||
          ($row->type == self::RULE_TYPE_JS  && !$row->has_js)) {
        continue;
      }
      if (empty($row->pages)) {
        $crids[] = $row->crid;
        continue;
      }

      if ($row->visibility == 2) {
        if (drupal_eval($row->pages)) {
          $crids[] = $row->crid;
        }
        continue;
      }

      $alias = drupal_get_path_alias($_GET['q']);
      // Compare with the internal and path alias (if any).
      if (drupal_match_path($alias, $row->pages) || ($alias != $_GET['q'] && drupal_match_path($_GET['q'], $row->pages))) {
        if ($row->visibility == 1) {
          $crids[] = $row->crid;
        }
      }
    }

    if (!$crids) {
      // Will not cache.
      return array();
    }

    $db_placeholders = db_placeholders($crids, 'int');
    $result = db_query("SELECT * FROM {jcss_insert_rules} WHERE crid IN (" . $db_placeholders . ")", $crids);

    while ($row = db_fetch_object($result)) {
      $this->ruleObjectPrepare($row);
      $ret[$row->crid] = $row;
    }
    return $ret;
  }

  /**
   * Prepare a rule object by adding properties to be used on insertion.
   *
   * No data will be written to the file pathes set here, that is the job of the
   * process function.
   *
   * Added properties:
   * 'file_js'
   * 'file_css'
   * 'file_css_rtl'
   * 'files'
   * 'evaluated'
   * 'result_css'
   * 'result_css_rtl'
   * 'result_js'
   *
   * @param $rule_object
   *   stdClass
   */
  public function ruleObjectPrepare(&$rule_object) {
    $rule_object->file_js = $rule_object->file_css = $rule_object->file_css_rtl = '';

    $rule_object->files = array();
    $rule_object->evaluated = FALSE;

    $rule_object->result_css     = $rule_object->css;
    $rule_object->result_css_rtl = $rule_object->css_rtl;
    $rule_object->result_js      = $rule_object->js;

    $directory  = $this->getSaveDir();
    $name       = drupal_strtolower(preg_replace('#[^A-z_.-]#', '-', $rule_object->title));

    if (!$rule_object->eval_php) {
      if ($rule_object->type == self::RULE_TYPE_CSS) {
        $rule_object->file_css     = "{$directory}/{$rule_object->crid}-{$name}.css";
        $rule_object->file_css_rtl = "{$directory}/{$rule_object->crid}-{$name}" . ($rule_object->rtl_standalone ? '-rtl_standalone.css' : '-rtl.css');

        $rule_object->files[] = $rule_object->file_css;
        $rule_object->files[] = $rule_object->file_css_rtl;
      }

      if ($rule_object->type == self::RULE_TYPE_JS) {
        $rule_object->file_js = "{$directory}/{$rule_object->crid}-{$name}.js";
        $rule_object->files[] = $rule_object->file_js;
      }
    }
  }

  /**
   * Evaluate the PHP code in CSS or JS code.
   *
   * If the rule object is set to use PHP evaluation, and the pathes to files
   * which will contain the result will differ according to the checksum of
   * the output.
   *
   * After the evaluation the object will be marked as evaluated so it is ready
   * to be passed to the process function.
   *
   * No data will be written to the file pathes set here, that is the job of the
   * process function.
   *
   * @param $rule_object
   *   stdClass
   */
  public function ruleObjectEvaluate(&$rule_object) {
    $rule_object->evaluated = TRUE;
    if (!$rule_object->eval_php) {
      return;
    }

    $directory  = $this->getSaveDir();
    $name       = drupal_strtolower(preg_replace('#[^A-z_.-]#', '-', $rule_object->title));

    if ($rule_object->type == self::RULE_TYPE_CSS) {
      $rule_object->result_css = $rule_object->css ? drupal_eval($rule_object->css) : '';
      if ($rule_object->result_css) {
        $md5 = md5($rule_object->result_css);
        $rule_object->file_css = "{$directory}/{$rule_object->crid}-{$name}-{$md5}.css";
        $rule_object->files[]  = $rule_object->file_css;
      }

      $rule_object->result_css_rtl = $rule_object->css_rtl ? drupal_eval($rule_object->css_rtl) : '';
      if ($rule_object->result_css_rtl) {
        $md5 = md5($rule_object->result_css_rtl);
        $rule_object->file_css_rtl  = "{$directory}/{$rule_object->crid}-{$name}-{$md5}" . ($rule_object->rtl_standalone ? '-rtl_standalone.css' : '-rtl.css');
        $rule_object->files[]       = $rule_object->file_css_rtl;
      }
    }

    if ($rule_object->type == self::RULE_TYPE_JS) {
      $rule_object->result_js = $rule_object->js ? drupal_eval($rule_object->js) : '';
      if ($rule_object->result_js) {
        $md5 = md5($rule_object->result_js);
        $rule_object->file_js = "{$directory}/{$rule_object->crid}-{$name}-{$md5}.js";
        $rule_object->files[] = $rule_object->file_js;
      }
    }
  }

  /**
   * Process a rule for CSS or JS inclusion.
   *
   * @param $rule_object
   *   stdClass
   */
  public function processRule($rule_object) {
    if ($rule_object->type == self::RULE_TYPE_CSS) {
      $this->ruleProcessCSS($rule_object);
    }
    if ($rule_object->type == self::RULE_TYPE_JS) {
      $this->ruleProcessJS($rule_object);
    }
  }

  /**
   * Process a rule for CSS inclusion type.
   *
   * @param $rule_object
   *   stdClass
   */
  public function ruleProcessCSS($rule_object) {
    if ($rule_object->type != self::RULE_TYPE_CSS) {
      return;
    }

    if (!$rule_object->evaluated) {
      $this->ruleObjectEvaluate($rule_object);
    }

    $is_rtl = $GLOBALS['language']->direction == LANGUAGE_RTL;

    $error = FALSE;
    while ($rule_object->result_css_rtl && $rule_object->file_css_rtl && $is_rtl) {
      $check = TRUE;
      if (!file_exists($rule_object->file_css_rtl)) {
        if (!$this->filePutContents($rule_object->file_css_rtl, $rule_object->result_css_rtl)) {
          $error = TRUE;
          break;
        }
        else {
          $check = FALSE;
        }
      }

      // Here file exists.
      if ($check) {
        switch (variable_get('jcss_insert_check_method', 'none')) {
          case 'none':
            break;

          case 'mtime':
            $diff  = $rule_object->updated - filemtime($rule_object->file_css_rtl);
            $allow = variable_get('jcss_insert_mtime_diff', 2);
            if ($diff + $allow > 0 && !$this->filePutContents($rule_object->file_css_rtl, $rule_object->result_css_rtl)) {
              $error = TRUE;
              break 2; // while
            }
            break;

          case 'mtime_size':
            $diff  = $rule_object->updated - filemtime($rule_object->file_css_rtl);
            $allow = variable_get('jcss_insert_mtime_diff', 2);
            if ($diff + $allow > 0 || filesize($rule_object->file_css_rtl) != drupal_strlen($rule_object->result_css_rtl)) {
              if (!$this->filePutContents($rule_object->file_css_rtl, $rule_object->result_css_rtl)) {
                $error = TRUE;
                break 2; // while
              }
            }
            break;

          case 'md5':
            if (md5_file($rule_object->file_css_rtl) != md5($rule_object->result_css_rtl)) {
              if (!$this->filePutContents($rule_object->file_css_rtl, $rule_object->result_css_rtl)) {
                $error = TRUE;
                break 2; // while
              }
            }
            break;
        }
      }

      // If the rule is not RTL stanalone and there is no normal CSS rule
      // to insert (later) then we will not insert this rule using
      // drupal_add_css() because it will be using the next call to that
      // function for normal CSS.
      if (!$rule_object->file_css || $rule_object->rtl_standalone) {
        drupal_add_css($rule_object->file_css_rtl, $rule_object->as_theme ? 'theme' : 'module', $rule_object->media, $rule_object->preprocess);
      }

      break; // It is not really a while(), it is an if () with goto()!
    }

    if ($error) {
      $vars = array('@title' => $rule_object->title, '@id' => '#' . $rule_object->crid, '@file' => $rule_object->file_css_rtl);
      watchdog('jcss_insert', 'Failed to update RTL CSS file for insert rule [@title](@id), error writing to file: [@file]', $vars, WATCHDOG_ERROR);
      if ($rule_object->report_errors == self::PROCESS_ERRORS_REPORT_YES || ($rule_object->report_errors == self::PROCESS_ERRORS_REPORT_MODULE_ADMIN && user_access('administer jcss insert'))) {
        drupal_set_message(t('Failed to update RTL CSS file for insert rule [@title](@id), error writing to file: [@file]', $vars), 'error');
      }
      file_delete($rule_object->file_css_rtl);
    }


    $error = FALSE;
    while ($rule_object->result_css && $rule_object->file_css && !($is_rtl && $rule_object->rtl_standalone)) {
      $check = TRUE;
      if (!file_exists($rule_object->file_css)) {
        if (!$this->filePutContents($rule_object->file_css, $rule_object->result_css)) {
          $error = TRUE;
          break;
        }
        else {
          $check = FALSE;
        }
      }
      // Here file exists.
      if ($check) {
        switch (variable_get('jcss_insert_check_method', 'none')) {
          case 'none':
            break;

          case 'mtime':
            $diff  = $rule_object->updated - filemtime($rule_object->file_css);
            $allow = variable_get('jcss_insert_mtime_diff', 2);
            if ($diff + $allow > 0) {
              if (!$this->filePutContents($rule_object->file_css, $rule_object->result_css)) {
                $error = TRUE;
                break 2; // while
              }
            }
            break;

          case 'mtime_size':
            $diff  = $rule_object->updated - filemtime($rule_object->file_css);
            $allow = variable_get('jcss_insert_mtime_diff', 2);
            if ($diff + $allow > 0 || filesize($rule_object->file_css) != drupal_strlen($rule_object->result_css)) {
              if (!$this->filePutContents($rule_object->file_css, $rule_object->result_css)) {
                $error = TRUE;
                break 2; // while
              }
            }
            break;

          case 'md5':
            if (md5_file($rule_object->file_css) != md5($rule_object->result_css)) {
              if (!$this->filePutContents($rule_object->file_css, $rule_object->result_css)) {
                $error = TRUE;
                break 2; // while
              }
            }
            break;
        }
      }
      drupal_add_css($rule_object->file_css, $rule_object->as_theme ? 'theme' : 'module', $rule_object->media, $rule_object->preprocess);
      break;
    }

    if ($error) {
      $vars = array(
        '@title'  => $rule_object->title,
        '@id'     => '#' . $rule_object->crid,
        '@file'   => $rule_object->file_css,
      );
      watchdog('jcss_insert', 'Failed to update CSS file for insert rule [@title](@id), error writing to file: [@file]', $vars, WATCHDOG_ERROR);
      if ($rule_object->report_errors == self::PROCESS_ERRORS_REPORT_YES || ($rule_object->report_errors == self::PROCESS_ERRORS_REPORT_MODULE_ADMIN && user_access('administer jcss insert'))) {
        drupal_set_message(t('Failed to update CSS file for insert rule [@title](@id), error writing to file: [@file]', $vars), 'error');
      }
      file_delete($rule_object->file_css);
    }
  }

  /**
   * Process a rule for JS inclusion type.
   *
   * @param $rule_object
   *   stdClass
   */
  public function ruleProcessJS($rule_object) {
    if ($rule_object->type != self::RULE_TYPE_JS) {
      return;
    }

    if (!$rule_object->evaluated) {
      $this->ruleObjectEvaluate($rule_object);
    }

    $error = FALSE;
    while ($rule_object->result_js && ($rule_object->inline || $rule_object->file_js)) {
      if ($rule_object->inline) {
        drupal_add_js($rule_object->result_js, 'inline', $rule_object->scope, FALSE, TRUE, $rule_object->preprocess);
        break;
      }

      $check = TRUE;
      if (!file_exists($rule_object->file_js)) {
        if (!$this->filePutContents($rule_object->file_js, $rule_object->result_js)) {
          $error = TRUE;
          break;
        }
        else {
          $check = FALSE;
        }
      }
      // Here file exists.
      if ($check) {
        switch (variable_get('jcss_insert_check_method', 'none')) {
          case 'none':
            break;

          case 'mtime':
            $diff  = $rule_object->updated - filemtime($rule_object->file_js);
            $allow = variable_get('jcss_insert_mtime_diff', 2);
            if ($diff + $allow > 0 && !$this->filePutContents($rule_object->file_js, $rule_object->result_js)) {
              $error = TRUE;
              break 2; // while.
            }
            break;

          case 'mtime_size':
            $diff  = $rule_object->updated - filemtime($rule_object->file_js);
            $allow = variable_get('jcss_insert_mtime_diff', 2);
            if ($diff + $allow > 0 || filesize($rule_object->file_js) != drupal_strlen($rule_object->result_js)) {
              if (!$this->filePutContents($rule_object->file_js, $rule_object->result_js)) {
                $error = TRUE;
                break 2; // while.
              }
            }
            break;

          case 'md5':
            if (md5_file($rule_object->file_js) != md5($rule_object->result_js)) {
              if (!$this->filePutContents($rule_object->file_js, $rule_object->result_js)) {
                $error = TRUE;
                break 2; // while.
              }
            }
            break;
        }
      }

      drupal_add_js($rule_object->file_js, $rule_object->as_theme ? 'theme' : 'module', $rule_object->scope, FALSE, TRUE, $rule_object->preprocess);

      // It is not really a while(), it is an if () with goto()!
      break;
    }

    if ($error) {
      $vars = array(
        '@title' => $rule_object->title,
        '@id'    => '#' . $rule_object->crid,
        '@file'  => $rule_object->file_js,
      );
      watchdog('jcss_insert', 'Failed to update JS file for insert rule [@title](@id), error writing to file: [@file]', $vars, WATCHDOG_ERROR);
      if ($rule_object->report_errors == self::PROCESS_ERRORS_REPORT_YES || ($rule_object->report_errors == self::PROCESS_ERRORS_REPORT_MODULE_ADMIN && user_access('administer jcss insert'))) {
        drupal_set_message(t('Failed to update JS file for insert rule [@title](@id), error writing to file: [@file]', $vars), 'error');
      }
      file_delete($rule_object->file_js);
    }
  }

  /**
   * Remove an existing rule and its accompanying file.
   *
   * @param $rule_object
   *   stdClass
   */
  public function ruleRemove($rule_object) {
    if (is_object($rule_object) && $rule_object->crid) {
      $this->ruleDeleteFiles($rule_object);
      db_query("DELETE FROM {jcss_insert_rules} WHERE crid = %d", $rule_object->crid);
    }
  }

  /**
   * Delete files related to a rule.
   *
   * @param $rule_object
   *   stdClass
   */
  public function ruleDeleteFiles($rule_object) {
    if (is_object($rule_object) && $rule_object->crid) {
      foreach ($rule_object->files as $file) {
        file_delete($file);
      }
    }
  }

  /**
   * Save a new or update an existing rule object into the database.
   *
   * Will also set the 'updated' property, and flush the caches.
   *
   * The only required property is the 'title' property which will cause a
   * record to be inserted or updated according to the existance of 'crid'
   * property in the $rule_object parameter.
   *
   * If $edit parameter is empty, values to save are taken from the passed rule
   * object as is without modification.
   *
   * The $edit parameter is supposed to be an array taken from FORM submission
   * (in $form_state['values']) where keys corresponds to properties of the
   * rule object.
   *
   * The only modification done for values in $edit if for the 'pages' property
   * (see 'jcss_insert' schema) where it is trimmed, beside, 'css', 'css_rtl',
   * and 'js' properties are inserted as empty strings if their trim() result is
   * empty, else they are inserted as provided.
   *
   * The 'updated' timestamp value is always set (for new and existing rules)
   * and has a value slightly greater than the current time().
   *
   * @param $rule_object
   *   stdClass populated or empty object which will be refereshed after an
   *   insert or update operation gets performed, if it has the 'crid' property
   *   set, it is considered an update operation, else a new record is inserted.
   *
   * @param $edit
   *   an array used to update the properties of $rule_object, all properties
   *   in $rule_object will be updated if $edit is passed except the 'crid'.
   *
   * @return
   *   TRUE on success, FALSE otherwise.
   */
  public function ruleSave(stdClass &$rule_object, $edit = NULL) {
    $update = isset($rule_object->crid) && $rule_object->crid;

    if ($update) {
      $this->ruleDeleteFiles($rule_object);
    }
    else {
      $rule_object->created = time();
    }
    $rule_object->updated = time() + ceil(variable_get('jcss_insert_mtime_diff', 2) / 2);

    if ($edit && is_array($edit)) {
      $rule_object->disabled        = (int) $edit['disabled'];
      $rule_object->type            = (int) $edit['type'];
      $rule_object->title           = $edit['title'];
      $rule_object->description     = $edit['description'];
      $rule_object->visibility      = (int) $edit['visibility'];
      $rule_object->pages           = trim($edit['pages']);
      $rule_object->media           = $edit['media'] ? $edit['media'] : 'all';
      $rule_object->preprocess      = (int) $edit['preprocess'];
      $rule_object->as_theme        = (int) $edit['as_theme'];
      $rule_object->css             = trim($edit['css']) ? $edit['css'] : '';
      $rule_object->css_rtl         = trim($edit['css_rtl']) ? $edit['css_rtl'] : '';
      $rule_object->rtl_standalone  = (int) $edit['rtl_standalone'];
      $rule_object->js              = trim($edit['js']) ? $edit['js'] : '';
      $rule_object->scope           = ($edit['scope'] != 'header' && $edit['scope'] != 'footer') ? 'header' : $edit['scope'];
      $rule_object->inline          = (int) $edit['inline'];
      $rule_object->eval_php        = (int) $edit['eval_php'];
      $rule_object->report_errors   = (int) $edit['report_errors'];
      $rule_object->weight          = (int) $edit['weight'];
    }
    if (!$rule_object->title) {
      return FALSE;
    }

    $success = (bool) drupal_write_record('jcss_insert_rules', $rule_object, $update ? 'crid' : array());

    if ($success) {
      $crid = $rule_object->crid;
      $rule_object = $this->ruleObjectsGetAll($rule_object->crid, TRUE);
      $rule_object = $rule_object[$crid];
      // Clear static cache.
      $this->ruleObjectsGetActive(TRUE);

      // Will not call $this->ruleObjectEvaluate($rule_object) here, because
      // the context may not be suitable for evaluating PHP code.
      if ($rule_object->file_css && $rule_object->result_css) {
        $this->filePutContents($rule_object->file_css, $rule_object->result_css);
      }

      if ($rule_object->file_css_rtl && $rule_object->result_css_rtl) {
        $this->filePutContents($rule_object->file_css_rtl, $rule_object->result_css_rtl);
      }

      if ($rule_object->file_js && $rule_object->result_js) {
        $this->filePutContents($rule_object->file_js, $rule_object->result_js);
      }
    }

    return $success;
  }

  /**
   * Helper which must be called to setup the required javascript used to
   * activate the quick edit form.
   */
  public function quickEditLoadRequirements() {
    t('Close full screen');
    t('CSS code');
    t('RTL CSS code');
    drupal_add_css($this->modulePath . '/assets/jcss-insert.css', 'module');
    drupal_add_js($this->modulePath . '/assets/jcss-insert-common.js', 'module');
    drupal_add_js($this->modulePath . '/assets/jcss-insert-qedit.js', 'module');

    drupal_add_js(array(
      'jcss_insert' => array(
        'qedit_url_prifix' => url('admin/settings/jcss-insert/qedit', array('absolute' => TRUE)),
      ),
    ), 'setting');
  }

  /**
   * Helper to insert a sample CSS rule, called by install hook.
   */
  public function insertSampleRule() {
    $rule_object = new stdClass();

    $rule_object->type  = self::RULE_TYPE_CSS;
    $rule_object->title = 'CSS/JS Sample Rule';
    $rule_object->description = 'This CSS rule will change the style of textareas in rule edit page without being hardcoded in the module CSS file';
    $rule_object->css = "#jcss-insert-admin-edit textarea.jcss-insert-code,\n" .
                        "#jcss-insert-admin-quick-edit textarea.jcss-insert-code {\n" .
                        "  color: #aa2222;\n" .
                        "  font-size: small;\n" .
                        "  font-weight: bold;\n" .
                        "}";
    // $this->ruleSave(() will not work on hook_install since it is using
    // drupal_write_record().
    db_query("INSERT INTO {jcss_insert_rules} (type, title, description, css) VALUES (%d, '%s', '%s', '%s')", $rule_object->type, $rule_object->title, $rule_object->description, $rule_object->css);
  }

  /**
   * Set or get the status of the block provided by the module.
   *
   * The block has to be disabled for 'admin/settings/jcss-insert/edit/%' path
   * to prevent CSS class names conflict.
   *
   * @param $status
   *   If NULL then the current status is return else the current status is
   *   set to the boolean value, NOTE that TRUE means the block is disabled.
   */
  public function blockDisabled($status = NULL) {
    if (NULL === $status) {
      return $this->blockDisabled;
    }
    return ($this->blockDisabled = (bool) $status);
  }
}
